# Provenance Lab

## Requirements
- golangci-lint

## Build and Package app
```bash
make build
```

## Lint app
```bash
make lint
```

## test app
```bash
make test
```